import React, { Component } from "react";
import { shoeArr } from "./data_shoesShop";
import ShoesItems from "./ShoesItems";
import TableGioHang from "./TableGioHang";

export default class Ex_ShoesShop extends Component {
  state = {
    shoeArr: shoeArr,
    gioHang: [],
  };
  renderShoes = () => {
    return this.state.shoeArr.map((item) => {
      return (
        <ShoesItems
          handleAddToCart={this.handleAddToCart}
          shoeData={item}
          key={item.id}
        />
      );
    });
  };

  handleAddToCart = (sp) => {
    //clone gio hang va them vao clone xong gan clone vao goc
    // let cloneGioHang = [...this.state.gioHang, shoes];
    // this.setState({ gioHang: cloneGioHang });

    // th1: sp chua co trong gio hang => tao object MIDIOutput, co them truong quantity trong gio hang
    //
    //th2:
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == sp.id;
    });
    let cloneGioHang = [...this.state.gioHang];
    if (index == -1) {
      let newSp = { ...sp, soLuong: 1 };
      cloneGioHang.push(newSp);
    } else {
      cloneGioHang[index].soLuong++;
    }
    this.setState({ gioHang: cloneGioHang });
  };

  handleDelete = (idshoe) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idshoe;
    });
    if (index !== -1) {
      let cloneGioHang = [...this.state.gioHang];
      cloneGioHang.splice(index, 1);
      this.setState({ gioHang: cloneGioHang });
    }
  };
  handleChangeQuantity = (idshoe, step) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idshoe;
    });
    let cloneGioHang = [...this.state.gioHang];
    cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + step;
    if (cloneGioHang[index].soLuong == 0) {
      cloneGioHang.splice(index, 1);
    }
    this.setState({ gioHang: cloneGioHang });
  };
  render() {
    return (
      <div className="container py-5">
        {this.state.gioHang.length > 0 && (
          <TableGioHang
            handleDelete={this.handleDelete}
            gioHang={this.state.gioHang}
            handleChangeQuantity={this.handleChangeQuantity}
          />
        )}

        <div className="row">{this.renderShoes()}</div>
      </div>
    );
  }
}
